﻿using LendFoundry.Syndication.Karza.Proxy;
using System;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using LendFoundry.EventHub;
using LendFoundry.Syndication.Karza.Events;
using LendFoundry.Foundation.Lookup;
using LendFoundry.Foundation.Services;
using LendFoundry.Foundation.Client;
using System.Linq;
using LendFoundry.Syndication.Karza.Request;
using LendFoundry.Syndication.Karza.Response;

namespace LendFoundry.Syndication.Karza
{
  
    public class KarzaService :IKarzaService
    {
        public KarzaService(IKarzaServiceConfiguration configurationservice,IKarzaProxy karzaproxy, IEventHubClient eventHub, ILookupService lookup)
        {
            if (configurationservice == null)
                throw new ArgumentNullException(nameof(configurationservice));
            if (karzaproxy == null)
                throw new ArgumentNullException(nameof(karzaproxy));
            if (string.IsNullOrWhiteSpace(configurationservice.BaseUrl))
                throw new ArgumentException("Url is required", nameof(configurationservice.BaseUrl));
            if (string.IsNullOrWhiteSpace(configurationservice.ApiKey))
                throw new ArgumentException("ApiKey is required", nameof(configurationservice.ApiKey));
            KarzaServiceConfiguration = configurationservice;
            KarzaProxy = karzaproxy;
            EventHub = eventHub;
            Lookup = lookup;
        }
        public static string ServiceName { get; } = "karza";

        private IKarzaServiceConfiguration KarzaServiceConfiguration { get; }
        private IKarzaProxy KarzaProxy { get; }
        private IEventHubClient EventHub { get; }
        private ILookupService Lookup { get; }
        

       public async Task<Response.IGSTAuthorizationResponse> GSTAuthorizationDetail(string entityType, string entityId,IGSTAuthenticationRequest gstauthenticationrequest)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentException("EntityType is require", nameof(entityType));
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException("EntityId is require", nameof(entityId));
            if(gstauthenticationrequest==null)
                throw new ArgumentException(nameof(gstauthenticationrequest));

            if (string.IsNullOrWhiteSpace(gstauthenticationrequest.consent))
                throw new ArgumentException("Gstin is require", nameof(gstauthenticationrequest.consent));
            if (string.IsNullOrWhiteSpace(gstauthenticationrequest.gstin))
                throw new ArgumentException("Gstin is require", nameof(gstauthenticationrequest.gstin));
            if (!(new Regex(@"^[0-9]{2}\w{5}\d{4}\w{1}\d{1}(\w{2}|\w{1}\d{1})$").IsMatch(gstauthenticationrequest.gstin)))
                throw new ArgumentException("Invalid gstin format", nameof(gstauthenticationrequest.gstin));
            try
            {
                entityType = EnsureEntityType(entityType);
                var proxyresponse = await KarzaProxy.GSTAuthorizationDetail(gstauthenticationrequest);
                var response=new Response.GSTAuthorizationResponse(proxyresponse);
                var referencenumber = Guid.NewGuid().ToString("N");
                await EventHub.Publish(new GSTAuthorizationDetailRequested
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    ReferenceNumber = referencenumber,
                    Response = response,
                    Request = gstauthenticationrequest,
                    Name = ServiceName
                });
                return response;
            }
            catch(KarzaException exception)
            {
                await EventHub.Publish(new GSTAuthorizationDetailRequestedFail
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    ReferenceNumber = null,
                    Response = exception.Message,
                    Request = gstauthenticationrequest,
                    Name = ServiceName
                });
                throw new KarzaException(exception.Message);
            }

        }
        
         public async Task<IPANAuthorizationResponse> PANAuthenticationDetail(string entityType, string entityId,IPANAuthenticationRequest panauthenticationrequest)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentException("EntityType is require", nameof(entityType));
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException("EntityId is require", nameof(entityId));
            if(panauthenticationrequest==null)
                throw new ArgumentException(nameof(panauthenticationrequest));

           
            if (string.IsNullOrWhiteSpace(panauthenticationrequest.pan))
                throw new ArgumentException("Pan is require", nameof(panauthenticationrequest.pan));
            if (!(new Regex(@"^[A-Za-z]{5}\d{4}[A-Za-z]{1}$").IsMatch(panauthenticationrequest.pan)))
                throw new ArgumentException("Invalid Pan format", nameof(panauthenticationrequest.pan));

            if (string.IsNullOrWhiteSpace(panauthenticationrequest.consent))
                 panauthenticationrequest.consent= KarzaServiceConfiguration.Consent;
            try
            {
                entityType = EnsureEntityType(entityType);
                var proxyresponse = await KarzaProxy.PANAuthenticationDetail(panauthenticationrequest);
                var response=new Response.PANAuthorizationResponse(proxyresponse);
                var referencenumber = Guid.NewGuid().ToString("N");
                await EventHub.Publish(new PANAuthorizationDetailRequested
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    ReferenceNumber = referencenumber,
                    Response = response,
                    Request = panauthenticationrequest,
                    Name = ServiceName
                });
                return response;
            }
            catch(KarzaException exception)
            {
                await EventHub.Publish(new PANAuthorizationDetailRequestedFail
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    ReferenceNumber = null,
                    Response = exception.Message,
                    Request = panauthenticationrequest,
                    Name = ServiceName
                });
              
              throw new KarzaException(exception.Message);
            }

        }
           public async Task<Response.IAadharAuthorizationResponse> AadharAuthenticationDetail(string entityType, string entityId,IAadharAuthenticationRequest aadharauthenticationrequest)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentException("EntityType is require", nameof(entityType));
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException("EntityId is require", nameof(entityId));
            if(aadharauthenticationrequest==null)
                throw new ArgumentException(nameof(aadharauthenticationrequest));

           
            if (string.IsNullOrWhiteSpace(aadharauthenticationrequest.AadhaarId))
                throw new ArgumentException("Aadhar is require", nameof(aadharauthenticationrequest.AadhaarId));
            if (!(new Regex(@"^[0-9]{12}$").IsMatch(aadharauthenticationrequest.AadhaarId)))
                throw new ArgumentException("Invalid Aadhar format", nameof(aadharauthenticationrequest.AadhaarId));

            if (string.IsNullOrWhiteSpace(aadharauthenticationrequest.Consent))
                 aadharauthenticationrequest.Consent= KarzaServiceConfiguration.Consent;
            try
            {
                entityType = EnsureEntityType(entityType);
                var proxyresponse = await KarzaProxy.AadharAuthenticationDetail(aadharauthenticationrequest);
                var response=new Response.AadharAuthorizationResponse(proxyresponse);
                var referencenumber = Guid.NewGuid().ToString("N");
              //  response.AadhaarStatusInfo= Lookup.GetLookupEntry("AadhaarStatusCode",response.AadhaarStatusCode).FirstOrDefault().Value;
                await EventHub.Publish(new AadharAuthorizationDetailRequested
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    ReferenceNumber = referencenumber,
                    Response = response,
                    Request = aadharauthenticationrequest,
                    Name = ServiceName
                });
                return response;
            }
            catch(KarzaException exception)
            {
                await EventHub.Publish(new AadharAuthorizationDetailRequestedFail
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    ReferenceNumber = null,
                    Response = exception.Message,
                    Request = aadharauthenticationrequest,
                    Name = ServiceName
                });
              
              throw new KarzaException(exception.Message);
            }

        }

       public async Task<Proxy.GSTTransactionAPIResponse> GSTTransactionAPIDetails(string entityType, string entityId,IGSTAuthenticationRequest gsttransactionrequest)
       {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentException("EntityType is require", nameof(entityType));
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException("EntityId is require", nameof(entityId));
            if(gsttransactionrequest==null)
                throw new ArgumentException(nameof(gsttransactionrequest));

            if (string.IsNullOrWhiteSpace(gsttransactionrequest.consent))
                throw new ArgumentException("Gstin is require", nameof(gsttransactionrequest.consent));
            if (string.IsNullOrWhiteSpace(gsttransactionrequest.gstin))
                throw new ArgumentException("Gstin is require", nameof(gsttransactionrequest.gstin));
            if (!(new Regex(@"^[0-9]{2}\w{5}\d{4}\w{1}\d{1}(\w{2}|\w{1}\d{1})$").IsMatch(gsttransactionrequest.gstin)))
                throw new ArgumentException("Invalid gstin format", nameof(gsttransactionrequest.gstin));
            try
            {
                entityType = EnsureEntityType(entityType);
                var response = await KarzaProxy.GSTTransactionAPIDetails(gsttransactionrequest);
                var referencenumber = Guid.NewGuid().ToString("N");
                await EventHub.Publish(new GSTTransactionRequested
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    ReferenceNumber = referencenumber,
                    Response = response,
                    Request = gsttransactionrequest,
                    Name = ServiceName
                });
                return response;
            }
            catch(KarzaException exception)
            {
                await EventHub.Publish(new GSTTransactionRequestedFail
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    ReferenceNumber = null,
                    Response = exception.Message,
                    Request = gsttransactionrequest,
                    Name = ServiceName
                });
                throw new KarzaException(exception.Message);
            }

        }
        
         
        public string EnsureEntityType(string entityType)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new InvalidArgumentException("Invalid Entity Type");

            entityType = entityType.ToLower();
            var validEntityTypes = Lookup.GetLookupEntry("entityTypes", entityType);

            if (validEntityTypes == null || !validEntityTypes.Any())
                throw new InvalidArgumentException("Invalid Entity Type");

            return entityType;
        }
    }
}
