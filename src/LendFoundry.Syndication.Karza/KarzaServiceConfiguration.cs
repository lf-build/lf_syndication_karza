﻿using System.Collections.Generic;
using LendFoundry.Foundation.Client;
namespace LendFoundry.Syndication.Karza
{
    public class KarzaServiceConfiguration : IKarzaServiceConfiguration, IDependencyConfiguration
    
    {

        public string ApiKey { get; set; } 

        public string BaseUrl { get; set; }
        public Dictionary<string,string> ServiceUrls{get;set;}
        public string Consent{get;set;}
        public Dictionary<string, string> Dependencies { get; set; }
        public string Database { get; set; }
        public string ConnectionString { get; set; }
         
    }
}