﻿using System.Threading.Tasks;
using LendFoundry.Syndication.Karza.Request;
using LendFoundry.Syndication.Karza.Response;

namespace LendFoundry.Syndication.Karza
{
    public interface IKarzaService
    {
        Task<IGSTAuthorizationResponse> GSTAuthorizationDetail(string entityType, string entityId, IGSTAuthenticationRequest gstauthenticationrequest);
        Task<IPANAuthorizationResponse> PANAuthenticationDetail(string entityType, string entityId, IPANAuthenticationRequest panuthenticationrequest);
        Task<IAadharAuthorizationResponse> AadharAuthenticationDetail(string entityType, string entityId, IAadharAuthenticationRequest aadharauthenticationrequest);
        Task<Proxy.GSTTransactionAPIResponse> GSTTransactionAPIDetails(string entityType, string entityId,IGSTAuthenticationRequest gsttransactionrequest);
        

    }
}