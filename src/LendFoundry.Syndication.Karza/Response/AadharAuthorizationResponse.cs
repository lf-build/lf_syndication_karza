using System.Collections.Generic;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Karza.Response
{
    public class AadharAuthorizationResponse : IAadharAuthorizationResponse
    {
        public AadharAuthorizationResponse(Proxy.IAadharAuthorizationResponse response)
        {
            if (response != null)
            {
                request_id = response.request_id;
                status_code = response.status_code;
                result = new AadharResult(response.result);
            }
        }
        public string status_code { get; set; }
        public string request_id { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IAadharResult, AadharResult>))]
        public IAadharResult result { get; set; }

    }
}
