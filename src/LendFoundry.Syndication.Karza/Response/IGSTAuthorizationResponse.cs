namespace LendFoundry.Syndication.Karza.Response
{
    public class IGSTAuthorizationResponse
    {
        IResult result { get; set; }
        string request_id { get; set; }
        string status_code { get; set; }
    }
}