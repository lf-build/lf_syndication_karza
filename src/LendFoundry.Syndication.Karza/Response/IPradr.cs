namespace LendFoundry.Syndication.Karza.Response
{
    public interface IPradr
    {
        string em { get; set; }
        string adr { get; set; }
        string addr { get; set; }
        long mb { get; set; }
        string ntr { get; set; }
        string lastUpdatedDate { get; set; }
    }
}