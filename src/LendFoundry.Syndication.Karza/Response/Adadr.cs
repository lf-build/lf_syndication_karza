using System.Collections.Generic;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Karza.Response
{

   
    public class Adadr:IAdadr
    {
        public Adadr()
        {
            
        }
        public Adadr(Proxy.Adadr addrResponse)
        {
            if(addrResponse!=null)
            {
                em=addrResponse.em;
                adr=addrResponse.adr;
                addr=addrResponse.addr;
                mb=addrResponse.mb;
                ntr=addrResponse.ntr;
                lastUpdatedDate=addrResponse.lastUpdatedDate;
            }
            
        }
        public string em { get; set; }
        public string adr { get; set; }
        public string addr { get; set; }
        public object mb { get; set; }
        public string ntr { get; set; }
        public string lastUpdatedDate { get; set; }
    }


}