namespace LendFoundry.Syndication.Karza.Response
{
    public interface IPANAuthorizationResponse
    {
        IPANAuthorizationResult result { get; set; }
        string request_id { get; set; }
        string status_code { get; set; }
    }
}