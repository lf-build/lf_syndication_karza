namespace LendFoundry.Syndication.Karza.Response
{
    public interface IAdadr
    {

         string em { get; set; }
         string adr { get; set; }
         string addr { get; set; }
         object mb { get; set; }
         string ntr { get; set; }
         string lastUpdatedDate { get; set; }
    }
}