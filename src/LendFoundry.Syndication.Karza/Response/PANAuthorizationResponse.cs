using System.Collections.Generic;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Karza.Response
{
    public class PANAuthorizationResponse:IPANAuthorizationResponse
    {
        public PANAuthorizationResponse(Proxy.PANAuthorizationResponse response)
        {
            if (response != null)
            {
                result = new PANAuthorizationResult(response.result);
                request_id = response.request_id;
                status_code = response.status_code;
            }
        }
        [JsonConverter(typeof(InterfaceConverter<IPANAuthorizationResult, PANAuthorizationResult>))]
        public IPANAuthorizationResult result { get; set; }
        public string request_id { get; set; }
        public string status_code { get; set; }
    }
}