using System.Collections.Generic;
using System.Linq;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Karza.Response
{

    public class Result : IResult
    {
          public Result()
            {
                
            }
        public Result(Proxy.Result result)
        {
          
            if (result != null)
            {
                mbr = result.mbr;
                canFlag = result.canFlag;
                pradr = new Pradr(result.pradr);
                tradeNam = result.tradeNam;
                contacted = result.contacted;
                gstin = result.gstin;
                nba = result.nba;
                stjCd = result.stjCd;
                stj = result.stj;
                ppr = result.ppr;
                dty = result.dty;
                cmpRt = result.cmpRt;
                lstupdt = result.lstupdt;
                ctb = result.ctb;
                sts = result.sts;
                cxdt = result.cxdt;
                adadr = result.adadr.Select(a=>new Adadr(a)).ToList<IAdadr>();
                lgnm = result.lgnm;
                ctjCd = result.ctjCd;
                ctj = result.ctj;
                rgdt = result.rgdt;
            }
        }
        public List<string> mbr { get; set; }
        public string canFlag { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IPradr, Pradr>))]
        public IPradr pradr { get; set; }
        public string tradeNam { get; set; }
        public string contacted { get; set; }
        public string gstin { get; set; }
        public List<string> nba { get; set; }
        public string stjCd { get; set; }
        public string stj { get; set; }
        public string ppr { get; set; }
        public string dty { get; set; }
        public string cmpRt { get; set; }
        public string lstupdt { get; set; }
        public string ctb { get; set; }
        public string sts { get; set; }
        public string cxdt { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IAdadr, Adadr>))]
        public List<IAdadr> adadr { get; set; }
        public string lgnm { get; set; }
        public string ctjCd { get; set; }
        public string ctj { get; set; }
        public string rgdt { get; set; }
    }


}