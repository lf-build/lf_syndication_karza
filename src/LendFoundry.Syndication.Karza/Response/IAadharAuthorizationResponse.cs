namespace LendFoundry.Syndication.Karza.Response
{
    public interface IAadharAuthorizationResponse
    {
        string status_code { get; set; }
        string request_id { get; set; }
        IAadharResult result { get; set; }

    }
}