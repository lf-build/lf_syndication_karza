using System.Collections.Generic;

namespace LendFoundry.Syndication.Karza.Response
{
    public interface IResult
    {
          List<string> mbr { get; set; }
         string canFlag { get; set; }
         IPradr pradr { get; set; }
         string tradeNam { get; set; }
         string contacted { get; set; }
         string gstin { get; set; }
         List<string> nba { get; set; }
         string stjCd { get; set; }
         string stj { get; set; }
         string ppr { get; set; }
         string dty { get; set; }
         string cmpRt { get; set; }
         string lstupdt { get; set; }
         string ctb { get; set; }
         string sts { get; set; }
         string cxdt { get; set; }
         List<IAdadr> adadr { get; set; }
         string lgnm { get; set; }
         string ctjCd { get; set; }
         string ctj { get; set; }
         string rgdt { get; set; }
    }
}