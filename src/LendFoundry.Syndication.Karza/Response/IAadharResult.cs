namespace LendFoundry.Syndication.Karza.Response
{
    public interface IAadharResult
    {
        string request_id { get; set; }
        bool success { get; set; }
        string AadhaarStatusCode { get; set; }
        string AadhaarReferenceCode { get; set; }
        string Info { get; set; }
        string PidTimestamp { get; set; }
        
    }
}