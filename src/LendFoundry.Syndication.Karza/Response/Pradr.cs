using System.Collections.Generic;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Karza.Response
{

    public class Pradr:IPradr
    {
        public Pradr()
        {
            
        }
        public Pradr(Proxy.Pradr padrResponse)
        {
            if(padrResponse!=null)
            {
                em=padrResponse.em;
                adr=padrResponse.adr;
                addr=padrResponse.addr;
                mb=padrResponse.mb;
                ntr=padrResponse.ntr;
                lastUpdatedDate=padrResponse.lastUpdatedDate;
            }
        }
        public string em { get; set; }
        public string adr { get; set; }
        public string addr { get; set; }
        public long mb { get; set; }
        public string ntr { get; set; }
        public string lastUpdatedDate { get; set; }
    }


}