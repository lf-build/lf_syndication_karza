using System.Collections.Generic;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Karza.Response
{
    public class PANAuthorizationResult : IPANAuthorizationResult
    {
        public PANAuthorizationResult(Proxy.PANAuthorizationResult result)
        {
            if(result!=null)
            {
                name=result.name;
            }
        }
        public string name { get; set; }
    }


}