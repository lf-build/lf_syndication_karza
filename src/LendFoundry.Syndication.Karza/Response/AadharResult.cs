using System.Collections.Generic;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Karza.Response
{
    public class AadharResult: IAadharResult
{
    public AadharResult(Proxy.AadhaarResult response)
    {
        if(response!=null)
        {
            request_id=response.request_id;
            success=response.success;
            AadhaarStatusCode=response.AadhaarStatusCode;
            AadhaarReferenceCode=response.AadhaarReferenceCode;
            Info=response.Info;
            PidTimestamp=response.PidTimestamp;
        }
    }
    public string request_id { get; set; }
    public bool success { get; set; }
    public string AadhaarStatusCode { get; set; }
    public string AadhaarReferenceCode { get; set; }
    public string Info { get; set; }
    public string PidTimestamp { get; set; }
    
}
}
