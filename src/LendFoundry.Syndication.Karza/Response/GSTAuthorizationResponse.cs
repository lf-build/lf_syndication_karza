using System.Collections.Generic;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Karza.Response
{
    
public class GSTAuthorizationResponse: IGSTAuthorizationResponse
{
    public GSTAuthorizationResponse(Proxy.IGSTAuthorizationResponse response)
    {
        if(response!=null)
        {
            result=new Result(response.result);
            request_id=response.request_id;
            status_code=response.status_code;
        }
        
    }
    [JsonConverter(typeof(InterfaceConverter<IResult, Result>))]
    public IResult result { get; set; }
    public string request_id { get; set; }
    public string status_code { get; set; }
}

}