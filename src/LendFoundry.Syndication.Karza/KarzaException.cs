﻿using System;
using System.Runtime.Serialization;

namespace LendFoundry.Syndication.Karza
{
    [Serializable]
    public class KarzaException : Exception
    {
        public KarzaException()
        {
        }

        public KarzaException(string message) : base(message)
        {
        }

        public KarzaException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected KarzaException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}