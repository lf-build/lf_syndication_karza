﻿using System.Collections.Generic;
using LendFoundry.Foundation.Client;
namespace LendFoundry.Syndication.Karza
{
    public interface  IKarzaServiceConfiguration : IDependencyConfiguration
    {

         string ApiKey { get; set; } 

         string BaseUrl { get; set; }
        Dictionary<string,string> ServiceUrls{get;set;}
        string Consent{get;set;}
         string ConnectionString { get; set; }
    }
}