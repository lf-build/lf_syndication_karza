using Newtonsoft.Json;

namespace LendFoundry.Syndication.Karza.Request
{

    public class Name : IName
    {
        public Name()
        {
            
        }
        public Name(Proxy.Request.IName request)
        {
            MatchingStrategy=request.MatchingStrategy;
            MatchingValue=request.MatchingValue;
            NameValue=request.NameValue;
        }
        public string MatchingStrategy { get; set; }
        public string MatchingValue { get; set; }
        public string NameValue { get; set; }
    }


}