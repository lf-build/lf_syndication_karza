using Newtonsoft.Json;

namespace LendFoundry.Syndication.Karza.Request
{
     public class GSTAuthenticationRequest:IGSTAuthenticationRequest
     {
     [JsonProperty("gstin")]
        public string gstin{get;set;}
     [JsonProperty("consent")]
        public string consent{get;set;}
     }
}