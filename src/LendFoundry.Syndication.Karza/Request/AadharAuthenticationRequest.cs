using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Karza.Request
{
    public class AadharAuthenticationRequest : IAadharAuthenticationRequest
    {
        public AadharAuthenticationRequest()
        {

        }
        public AadharAuthenticationRequest(LendFoundry.Syndication.Karza.Proxy.Request.IAadharAuthenticationRequest request)
        {
            AadhaarId=request.AadhaarId;
            Consent=request.Consent;
            Demographics=new Demographics(request.Demographics);
        }
        public string AadhaarId { get; set; }
        public string Consent { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IDemographics, Demographics>))]
        public IDemographics Demographics { get; set; }
    }


}