namespace LendFoundry.Syndication.Karza.Request
{
    public interface IName
    {
         string MatchingStrategy { get; set; }
         string MatchingValue { get; set; }
         string NameValue { get; set; }
    }
}