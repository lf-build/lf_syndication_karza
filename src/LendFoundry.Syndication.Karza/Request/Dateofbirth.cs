using Newtonsoft.Json;

namespace LendFoundry.Syndication.Karza.Request
{

    public class Dateofbirth : IDateofbirth
    {
        public Dateofbirth()
        {

        }
        public Dateofbirth(LendFoundry.Syndication.Karza.Proxy.Request.IDateofbirth request)
        {
            Format = request.Format;
            DobType = request.DobType;
        }
        public string Format { get; set; }
        public string DobType { get; set; }
        public string DobValue { get; set; }
    }


}