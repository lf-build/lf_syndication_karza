using Newtonsoft.Json;

namespace LendFoundry.Syndication.Karza.Request
{



    public class AddressFreetext:IAddressFreetext
    {
        public AddressFreetext()
        {
            
        }
        public AddressFreetext(LendFoundry.Syndication.Karza.Proxy.Request.IAddressFreetext request)
        {
           MatchingStrategy= request.MatchingStrategy;
           MatchingValue= request.MatchingValue;
           AddressValue= request.AddressValue;
        }
        public string MatchingStrategy { get; set; }
        public string MatchingValue { get; set; }
        public string AddressValue { get; set; }
    }


}