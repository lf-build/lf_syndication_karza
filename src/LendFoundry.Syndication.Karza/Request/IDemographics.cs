namespace LendFoundry.Syndication.Karza.Request
{
    public interface IDemographics
    {
        IName Name { get; set; }
        IDateofbirth Dob { get; set; }
        string Phone { get; set; }
        string Gender { get; set; }
        string AddressFormat { get; set; }
        IAddressFreetext AddressFreetext { get; set; }
    }
}