using Newtonsoft.Json;

namespace LendFoundry.Syndication.Karza.Request
{
     public class PANAuthenticationRequest:IPANAuthenticationRequest
     {
     [JsonProperty("pan")]
        public string pan{get;set;}
     [JsonProperty("consent")]
        public string consent{get;set;}
     }
}