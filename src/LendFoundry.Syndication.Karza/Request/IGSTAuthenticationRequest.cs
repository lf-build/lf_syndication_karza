namespace LendFoundry.Syndication.Karza.Request
{
    public interface IGSTAuthenticationRequest
    {
         string gstin{get;set;}
         string consent{get;set;}
    }
}