namespace LendFoundry.Syndication.Karza.Request
{
    public interface IPANAuthenticationRequest
    {
         string pan{get;set;}
         string consent{get;set;}
    }
}