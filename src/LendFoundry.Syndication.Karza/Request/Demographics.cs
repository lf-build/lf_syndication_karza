using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Karza.Request
{
    public class Demographics : IDemographics
    {
        public Demographics()
        {

        }
        public Demographics(LendFoundry.Syndication.Karza.Proxy.Request.IDemographics request)
        {
            if (request != null)
            {
                Name = new Name(request.Name);
                Dob = new Dateofbirth(request.Dob);
                Phone = request.Phone;
                Gender = request.Gender;
                AddressFormat = request.AddressFormat;
                AddressFreetext = new AddressFreetext(request.AddressFreetext);
            }

        }

        [JsonConverter(typeof(InterfaceConverter<IName, Name>))]
        public IName Name { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IDateofbirth, Dateofbirth>))]
        public IDateofbirth Dob { get; set; }
        public string Phone { get; set; }
        public string Gender { get; set; }
        public string AddressFormat { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IAddressFreetext, AddressFreetext>))]
        public IAddressFreetext AddressFreetext { get; set; }
    }


}