namespace LendFoundry.Syndication.Karza.Request
{
    public interface IAddressFreetext
    {
        string MatchingStrategy { get; set; }
        string MatchingValue { get; set; }
        string AddressValue { get; set; }
    }
}