using System.Collections.Generic;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Karza.Proxy
{
    public class AadhaarResult
{
    [JsonProperty("request_id")]
    public string request_id { get; set; }
    [JsonProperty("success")]
    public bool success { get; set; }
    [JsonProperty("aadhaar-status-code")]
    public string AadhaarStatusCode { get; set; }
    [JsonProperty("aadhaar-reference-code")]
    public string AadhaarReferenceCode { get; set; }
    [JsonProperty("info")]
    public string Info { get; set; }
    [JsonProperty("pid-timestamp")]
    public string PidTimestamp { get; set; }
}
public class AadharAuthorizationResponse:IAadharAuthorizationResponse
{
    [JsonProperty("pid-timestamp")]
    public string status_code { get; set; }
    [JsonProperty("request_id")]
    public string request_id { get; set; }
    [JsonProperty("result")]
    public AadhaarResult result { get; set; }
}
}
