namespace LendFoundry.Syndication.Karza.Proxy
{
    public interface IGSTAuthorizationResponse
    {
        Result result { get; set; }
        string request_id { get; set; }
        string status_code { get; set; }
    }
}