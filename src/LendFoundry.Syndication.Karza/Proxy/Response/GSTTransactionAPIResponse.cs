using System.Collections.Generic;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Karza.Proxy
{

    public class Contacted
{
    [JsonProperty("mobNum")]
    public long mobNum { get; set; }
    [JsonProperty("name")]
    public string name { get; set; }
    [JsonProperty("email")]
    public string email { get; set; }
}

public class DocumentDetail
{
    [JsonProperty("frmno")]
    public string frmno { get; set; }
    [JsonProperty("doc_url")]
    public string doc_url { get; set; }
    [JsonProperty("isdt")]
    public string isdt { get; set; }
    [JsonProperty("frmdc")]
    public string frmdc { get; set; }
}

public class Profile
{
    [JsonProperty("contacted")]
    public Contacted contacted { get; set; }
    [JsonProperty("cmpRt")]
    public string cmpRt { get; set; }
    [JsonProperty("rgdt")]
    public string rgdt { get; set; }
    [JsonProperty("tradeNam")]
    public string tradeNam { get; set; }
    [JsonProperty("nba")]
    public List<string> nba { get; set; }
    [JsonProperty("mbr")]
    public List<string> mbr { get; set; }
    [JsonProperty("document_detail")]
    public List<DocumentDetail> document_detail { get; set; }
    [JsonProperty("ctb")]
    public string ctb { get; set; }
    [JsonProperty("sts")]
    public string sts { get; set; }
    [JsonProperty("lgnm")]
    public string lgnm { get; set; }
}

public class Status
{
    [JsonProperty("status")]
    public string status { get; set; }
    [JsonProperty("tileDisable")]
    public bool tileDisable { get; set; }
    [JsonProperty("due_dt")]
    public string due_dt { get; set; }
    [JsonProperty("return_ty")]
    public string return_ty { get; set; }
}

public class FilingStatu
{
    [JsonProperty("status")]
    public List<Status> status { get; set; }
    [JsonProperty("ret_period")]
    public string ret_period { get; set; }
}

public class Payment
{
    [JsonProperty("total")]
    public int total { get; set; }
    [JsonProperty("tt_csh_pd")]
    public int tt_csh_pd { get; set; }
    [JsonProperty("tt_pay")]
    public int tt_pay { get; set; }
    [JsonProperty("tt_itc_pd")]
    public int tt_itc_pd { get; set; }
}

public class TotalTaxLiability
{
    [JsonProperty("total")]
    public double total { get; set; }
    [JsonProperty("csamt")]
    public int csamt { get; set; }
    [JsonProperty("samt")]
    public double samt { get; set; }
    [JsonProperty("iamt")]
    public double iamt { get; set; }
    [JsonProperty("camt")]
    public double camt { get; set; }
}

public class TransactionSummary
{
    [JsonProperty("gt")]
    public double gt { get; set; }
    [JsonProperty("payable")]
    public int payable { get; set; }
    [JsonProperty("period_to")]
    public string period_to { get; set; }
    [JsonProperty("period_from")]
    public string period_from { get; set; }
    [JsonProperty("cur_gt")]
    public int cur_gt { get; set; }
    [JsonProperty("turnover")]
    public double turnover { get; set; }
    [JsonProperty("payment")]
    public Payment payment { get; set; }
    [JsonProperty("total_tax_liability")]
    public TotalTaxLiability total_tax_liability { get; set; }
}

public class Transactions
{
    [JsonProperty("ttl_sgst")]
    public int ttl_sgst { get; set; }
    [JsonProperty("ttl_igst")]
    public double ttl_igst { get; set; }
    [JsonProperty("ttl_cess")]
    public int ttl_cess { get; set; }
    [JsonProperty("ttl_cgst")]
    public int ttl_cgst { get; set; }
    [JsonProperty("ttl_rec")]
    public int ttl_rec { get; set; }
    [JsonProperty("ttl_tax")]
    public double ttl_tax { get; set; }
    [JsonProperty("ttl_val")]
    public double ttl_val { get; set; }
}

public class StateWise
{
    [JsonProperty("state_code")]
    public string state_code { get; set; }
    [JsonProperty("transactions")]
    public Transactions transactions { get; set; }
}

public class Transactions2
{
    [JsonProperty("ttl_sgst")]
    public double ttl_sgst { get; set; }
    [JsonProperty("ttl_igst")]
    public double ttl_igst { get; set; }
    [JsonProperty("ttl_cess")]
    public int ttl_cess { get; set; }
    [JsonProperty("ttl_cgst")]
    public double ttl_cgst { get; set; }
    [JsonProperty("ttl_rec")]
    public int ttl_rec { get; set; }
    [JsonProperty("ttl_tax")]
    public int ttl_tax { get; set; }
    [JsonProperty("ttl_val")]
    public double ttl_val { get; set; }
}

public class CptySum
{
    [JsonProperty("pan")]
    public string pan { get; set; }
    [JsonProperty("transactions")]
    public Transactions2 transactions { get; set; }
}

public class Transactions3
{
    [JsonProperty("txval")]
    public double txval { get; set; }
    [JsonProperty("val")]
    public double val { get; set; }
    [JsonProperty("camt")]
    public double camt { get; set; }
    [JsonProperty("num")]
    public int num { get; set; }
    [JsonProperty("iamt")]
    public double iamt { get; set; }
    [JsonProperty("qty")]
    public double qty { get; set; }
    [JsonProperty("samt")]
    public double samt { get; set; }
}

public class Transactions4
{
    [JsonProperty("txval")]
    public double txval { get; set; }
    [JsonProperty("val")]
    public double val { get; set; }
    [JsonProperty("camt")]
    public double camt { get; set; }
    [JsonProperty("num")]
    public int num { get; set; }
    [JsonProperty("iamt")]
    public double iamt { get; set; }
    [JsonProperty("qty")]
    public double qty { get; set; }
    [JsonProperty("samt")]
    public double samt { get; set; }
}

public class DescWise
{
    [JsonProperty("transactions")]
    public Transactions4 transactions { get; set; }
    [JsonProperty("desc")]
    public string desc { get; set; }
}

public class HsnWise
{
    [JsonProperty("hsn_sc")]
    public string hsn_sc { get; set; }
    [JsonProperty("transactions")]
    public Transactions3 transactions { get; set; }
    [JsonProperty("hsn_desc")]
    public string hsn_desc { get; set; }
    [JsonProperty("desc_wise")]
    public List<DescWise> desc_wise { get; set; }
}

public class SecSum
{
    [JsonProperty("ttl_sgst")]
    public double ttl_sgst { get; set; }
    [JsonProperty("ttl_cess")]
    public int ttl_cess { get; set; }
    [JsonProperty("chksum")]
    public string chksum { get; set; }
    [JsonProperty("ttl_rec")]
    public int ttl_rec { get; set; }
    [JsonProperty("ttl_cgst")]
    public double ttl_cgst { get; set; }
    [JsonProperty("sec_nm")]
    public string sec_nm { get; set; }
    [JsonProperty("ttl_tax")]
    public double ttl_tax { get; set; }
    [JsonProperty("ttl_igst")]
    public double ttl_igst { get; set; }
    [JsonProperty("ttl_val")]
    public double ttl_val { get; set; }
    [JsonProperty("ttl_doc_issued")]
    public int? ttl_doc_issued { get; set; }
    [JsonProperty("net_doc_issued")]
    public int? net_doc_issued { get; set; }
    [JsonProperty("ttl_doc_cancelled")]
    public int? ttl_doc_cancelled { get; set; }
    [JsonProperty("ttl_ngsup_amt")]
    public int? ttl_ngsup_amt { get; set; }
    [JsonProperty("ttl_expt_amt")]
    public int? ttl_expt_amt { get; set; }
    [JsonProperty("ttl_nilsup_amt")]
    public int? ttl_nilsup_amt { get; set; }
}

public class Total
{
    [JsonProperty("state_wise")]
    public List<StateWise> state_wise { get; set; }
    [JsonProperty("cpty_sum")]
    public List<CptySum> cpty_sum { get; set; }
    [JsonProperty("hsn_wise")]
    public List<HsnWise> hsn_wise { get; set; }
    [JsonProperty("sec_sum")]
    public List<SecSum> sec_sum { get; set; }
}

public class HsnWise2
{
    [JsonProperty("txval")]
    public double txval { get; set; }
    [JsonProperty("val")]
    public int val { get; set; }
    [JsonProperty("uqc")]
    public string uqc { get; set; }
    [JsonProperty("qty")]
    public double qty { get; set; }
    [JsonProperty("camt")]
    public double camt { get; set; }
    [JsonProperty("csamt")]
    public int csamt { get; set; }
    [JsonProperty("num")]
    public int num { get; set; }
    [JsonProperty("iamt")]
    public double iamt { get; set; }
    [JsonProperty("desc")]
    public string desc { get; set; }
    [JsonProperty("hsn_sc")]
    public string hsn_sc { get; set; }
    [JsonProperty("samt")]
    public double samt { get; set; }
}

public class CptySum2
{
    [JsonProperty("ttl_sgst")]
    public double ttl_sgst { get; set; }
    [JsonProperty("ttl_igst")]
    public double ttl_igst { get; set; }
    [JsonProperty("ttl_cess")]
    public int ttl_cess { get; set; }
    [JsonProperty("ctin")]
    public string ctin { get; set; }
    [JsonProperty("ttl_cgst")]
    public double ttl_cgst { get; set; }
    [JsonProperty("chksum")]
    public string chksum { get; set; }
    [JsonProperty("ttl_rec")]
    public int ttl_rec { get; set; }
    [JsonProperty("ttl_tax")]
    public double ttl_tax { get; set; }
    [JsonProperty("ttl_val")]
    public int ttl_val { get; set; }
}

public class SecSum2
{
    [JsonProperty("ttl_sgst")]
    public double ttl_sgst { get; set; }
    [JsonProperty("sec_nm")]
    public string sec_nm { get; set; }
    [JsonProperty("ttl_igst")]
    public double ttl_igst { get; set; }
    [JsonProperty("ttl_cess")]
    public int ttl_cess { get; set; }
    [JsonProperty("ttl_cgst")]
    public double ttl_cgst { get; set; }
    [JsonProperty("chksum")]
    public string chksum { get; set; }
    [JsonProperty("ttl_rec")]
    public int ttl_rec { get; set; }
    [JsonProperty("ttl_tax")]
    public double ttl_tax { get; set; }
    [JsonProperty("ttl_val")]
    public int ttl_val { get; set; }
    [JsonProperty("ttl_doc_issued")]
    public int? ttl_doc_issued { get; set; }
    [JsonProperty("net_doc_issued")]
    public int? net_doc_issued { get; set; }
    [JsonProperty("ttl_doc_cancelled")]
    public int? ttl_doc_cancelled { get; set; }
    [JsonProperty("ttl_ngsup_amt")]
    public int? ttl_ngsup_amt { get; set; }
    [JsonProperty("ttl_expt_amt")]
    public int? ttl_expt_amt { get; set; }
    [JsonProperty("ttl_nilsup_amt")]
    public int? ttl_nilsup_amt { get; set; }
    [JsonProperty("cpty_sum")]
    public List<CptySum2> cpty_sum { get; set; }
}

public class Detail
{
    [JsonProperty("ret_period")]
    public string ret_period { get; set; }
    [JsonProperty("hsn_wise")]
    public List<HsnWise2> hsn_wise { get; set; }
    [JsonProperty("chksum")]
    public string chksum { get; set; }
    [JsonProperty("sec_sum")]
    public List<SecSum2> sec_sum { get; set; }
    [JsonProperty("b2bLimitReached")]
    public bool b2bLimitReached { get; set; }
    [JsonProperty("time")]
    public string time { get; set; }
}

public class Gstr1
{
    [JsonProperty("total")]
    public Total total { get; set; }
    [JsonProperty("details")]
    public List<Detail> details { get; set; }
}

public class Top3Cus
{
    [JsonProperty("ttl_rec")]
    public int ttl_rec { get; set; }
    [JsonProperty("share")]
    public double share { get; set; }
    [JsonProperty("ttl_tax")]
    public double ttl_tax { get; set; }
    [JsonProperty("pan")]
    public string pan { get; set; }
}

public class Averages
{
    [JsonProperty("avgmonthtax")]
    public double avgmonthtax { get; set; }
    [JsonProperty("avgttlcust")]
    public double avgttlcust { get; set; }
    [JsonProperty("avgrate")]
    public double avgrate { get; set; }
    [JsonProperty("avginvcust")]
    public int avginvcust { get; set; }
    [JsonProperty("avgqtycust")]
    public double avgqtycust { get; set; }
    [JsonProperty("avginv")]
    public double avginv { get; set; }
    [JsonProperty("avgqtyinv")]
    public double avgqtyinv { get; set; }
}

public class OsupDet
{
    [JsonProperty("csamt")]
    public int csamt { get; set; }
    [JsonProperty("camt")]
    public double camt { get; set; }
    [JsonProperty("samt")]
    public double samt { get; set; }
    [JsonProperty("txval")]
    public double txval { get; set; }
    [JsonProperty("iamt")]
    public double iamt { get; set; }
}

public class OsupNongst
{
    [JsonProperty("csamt")]
    public int csamt { get; set; }
    [JsonProperty("camt")]
    public int camt { get; set; }
    [JsonProperty("samt")]
    public int samt { get; set; }
    [JsonProperty("txval")]
    public int txval { get; set; }
    [JsonProperty("iamt")]
    public int iamt { get; set; }
}

public class OsupNilExmp
{
    [JsonProperty("csamt")]
    public int csamt { get; set; }
    [JsonProperty("camt")]
    public int camt { get; set; }
    [JsonProperty("samt")]
    public int samt { get; set; }
    [JsonProperty("txval")]
    public int txval { get; set; }
    [JsonProperty("iamt")]
    public int iamt { get; set; }
}

public class IsupRev
{
    [JsonProperty("csamt")]
    public int csamt { get; set; }
    [JsonProperty("camt")]
    public int camt { get; set; }
    [JsonProperty("samt")]
    public int samt { get; set; }
    [JsonProperty("txval")]
    public int txval { get; set; }
    [JsonProperty("iamt")]
    public int iamt { get; set; }
}

public class OsupZero
{
    [JsonProperty("csamt")]
    public int csamt { get; set; }
    [JsonProperty("camt")]
    public int camt { get; set; }
    [JsonProperty("samt")]
    public int samt { get; set; }
    [JsonProperty("txval")]
    public int txval { get; set; }
    [JsonProperty("iamt")]
    public int iamt { get; set; }
}

public class SupDetails
{
    [JsonProperty("osup_det")]
    public OsupDet osup_det { get; set; }
    [JsonProperty("osup_nongst")]
    public OsupNongst osup_nongst { get; set; }
    [JsonProperty("osup_nil_exmp")]
    public OsupNilExmp osup_nil_exmp { get; set; }
    [JsonProperty("isup_rev")]
    public IsupRev isup_rev { get; set; }
    [JsonProperty("osup_zero")]
    public OsupZero osup_zero { get; set; }
}

public class TtVal
{
    [JsonProperty("tt_csh_pd")]
    public int tt_csh_pd { get; set; }
    [JsonProperty("tt_pay")]
    public int tt_pay { get; set; }
    [JsonProperty("tt_itc_pd")]
    public int tt_itc_pd { get; set; }
}

public class LtfeeDetails
{
    [JsonProperty("csamt")]
    public int csamt { get; set; }
    [JsonProperty("samt")]
    public int samt { get; set; }
    [JsonProperty("camt")]
    public int camt { get; set; }
    [JsonProperty("iamt")]
    public int iamt { get; set; }
}

public class IntrLtfee
{
    [JsonProperty("ltfee_details")]
    public LtfeeDetails ltfee_details { get; set; }
}

public class UinDetail
{
    [JsonProperty("iamt")]
    public int iamt { get; set; }
    [JsonProperty("txval")]
    public int txval { get; set; }
}

public class CompSummary
{
    [JsonProperty("iamt")]
    public int iamt { get; set; }
    [JsonProperty("txval")]
    public int txval { get; set; }
    [JsonProperty("pos")]
    public string pos { get; set; }
}

public class CompDetail
{
    [JsonProperty("iamt")]
    public int iamt { get; set; }
    [JsonProperty("txval")]
    public int txval { get; set; }
}

public class UinSummary
{
    [JsonProperty("iamt")]
    public int iamt { get; set; }
    [JsonProperty("txval")]
    public int txval { get; set; }
    [JsonProperty("pos")]
    public string pos { get; set; }
}

public class InterSup
{
    [JsonProperty("uin_details")]
    public List<UinDetail> uin_details { get; set; }
    [JsonProperty("comp_summary")]
    public List<CompSummary> comp_summary { get; set; }
    [JsonProperty("comp_details")]
    public List<CompDetail> comp_details { get; set; }
    [JsonProperty("uin_summary")]
    public List<UinSummary> uin_summary { get; set; }
    [JsonProperty("unreg_details")]
    public List<object> unreg_details { get; set; }
}

public class ItcNet
{
    [JsonProperty("csamt")]
    public int csamt { get; set; }
    [JsonProperty("samt")]
    public double samt { get; set; }
    [JsonProperty("camt")]
    public double camt { get; set; }
    [JsonProperty("iamt")]
    public double iamt { get; set; }
}

public class ItcRev
{
    [JsonProperty("csamt")]
    public int csamt { get; set; }
    [JsonProperty("samt")]
    public int samt { get; set; }
    [JsonProperty("camt")]
    public int camt { get; set; }
    [JsonProperty("iamt")]
    public int iamt { get; set; }
    [JsonProperty("ty")]
    public string ty { get; set; }
}

public class ItcInelg
{
    [JsonProperty("csamt")]
    public int csamt { get; set; }
    [JsonProperty("samt")]
    public int samt { get; set; }
    [JsonProperty("camt")]
    public int camt { get; set; }
    [JsonProperty("iamt")]
    public int iamt { get; set; }
    [JsonProperty("ty")]
    public string ty { get; set; }
}

public class ItcAvl
{
    [JsonProperty("csamt")]
    public int csamt { get; set; }
    [JsonProperty("samt")]
    public double samt { get; set; }
    [JsonProperty("camt")]
    public double camt { get; set; }
    [JsonProperty("iamt")]
    public double iamt { get; set; }
    [JsonProperty("ty")]
    public string ty { get; set; }
}

public class ItcElg
{
    [JsonProperty("itc_net")]
    public ItcNet itc_net { get; set; }
    [JsonProperty("itc_rev")]
    public List<ItcRev> itc_rev { get; set; }
    [JsonProperty("itc_inelg")]
    public List<ItcInelg> itc_inelg { get; set; }
    [JsonProperty("itc_avl")]
    public List<ItcAvl> itc_avl { get; set; }
}

public class Total2
{
    [JsonProperty("sup_details")]
    public SupDetails sup_details { get; set; }
    [JsonProperty("tt_val")]
    public TtVal tt_val { get; set; }
    [JsonProperty("intr_ltfee")]
    public IntrLtfee intr_ltfee { get; set; }
    [JsonProperty("inter_sup")]
    public InterSup inter_sup { get; set; }
    [JsonProperty("itc_elg")]
    public ItcElg itc_elg { get; set; }
}

public class IsupRev2
{
    [JsonProperty("csamt")]
    public int csamt { get; set; }
    [JsonProperty("iamt")]
    public int iamt { get; set; }
    [JsonProperty("samt")]
    public int samt { get; set; }
    [JsonProperty("txval")]
    public int txval { get; set; }
    [JsonProperty("camt")]
    public int camt { get; set; }
}

public class OsupNongst2
{
    [JsonProperty("csamt")]
    public int csamt { get; set; }
    [JsonProperty("iamt")]
    public int iamt { get; set; }
    [JsonProperty("samt")]
    public int samt { get; set; }
    [JsonProperty("txval")]
    public int txval { get; set; }
    [JsonProperty("camt")]
    public int camt { get; set; }
}

public class OsupZero2
{
    [JsonProperty("csamt")]
    public int csamt { get; set; }
    [JsonProperty("iamt")]
    public int iamt { get; set; }
    [JsonProperty("samt")]
    public int samt { get; set; }
    [JsonProperty("txval")]
    public int txval { get; set; }
    [JsonProperty("camt")]
    public int camt { get; set; }
}

public class OsupNilExmp2
{
    [JsonProperty("csamt")]
    public int csamt { get; set; }
    [JsonProperty("iamt")]
    public int iamt { get; set; }
    [JsonProperty("samt")]
    public int samt { get; set; }
    [JsonProperty("txval")]
    public int txval { get; set; }
    [JsonProperty("camt")]
    public int camt { get; set; }
}

public class OsupDet2
{
    [JsonProperty("csamt")]
    public int csamt { get; set; }
    [JsonProperty("iamt")]
    public double iamt { get; set; }
    [JsonProperty("samt")]
    public double samt { get; set; }
    [JsonProperty("txval")]
    public double txval { get; set; }
    [JsonProperty("camt")]
    public double camt { get; set; }
}

public class SupDetails2
{
    [JsonProperty("isup_rev")]
    public IsupRev2 isup_rev { get; set; }
    [JsonProperty("osup_nongst")]
    public OsupNongst2 osup_nongst { get; set; }
    [JsonProperty("osup_zero")]
    public OsupZero2 osup_zero { get; set; }
    [JsonProperty("osup_nil_exmp")]
    public OsupNilExmp2 osup_nil_exmp { get; set; }
    [JsonProperty("osup_det")]
    public OsupDet2 osup_det { get; set; }
}

public class ItcNet2
{
    [JsonProperty("csamt")]
    public int csamt { get; set; }
    [JsonProperty("iamt")]
    public double iamt { get; set; }
    [JsonProperty("samt")]
    public double samt { get; set; }
    [JsonProperty("camt")]
    public double camt { get; set; }
}

public class ItcInelg2
{
    [JsonProperty("csamt")]
    public int csamt { get; set; }
    [JsonProperty("iamt")]
    public int iamt { get; set; }
    [JsonProperty("camt")]
    public int camt { get; set; }
    [JsonProperty("samt")]
    public int samt { get; set; }
    [JsonProperty("ty")]
    public string ty { get; set; }
}

public class ItcRev2
{
    [JsonProperty("csamt")]
    public int csamt { get; set; }
    [JsonProperty("iamt")]
    public int iamt { get; set; }
    [JsonProperty("camt")]
    public int camt { get; set; }
    [JsonProperty("samt")]
    public int samt { get; set; }
    [JsonProperty("ty")]
    public string ty { get; set; }
}

public class ItcAvl2
{
    [JsonProperty("csamt")]
    public int csamt { get; set; }
    [JsonProperty("iamt")]
    public double iamt { get; set; }
    [JsonProperty("camt")]
    public double camt { get; set; }
    [JsonProperty("samt")]
    public double samt { get; set; }
    [JsonProperty("ty")]
    public string ty { get; set; }
}

public class ItcElg2
{
    [JsonProperty("itc_net")]
    public ItcNet2 itc_net { get; set; }
    [JsonProperty("itc_inelg")]
    public List<ItcInelg2> itc_inelg { get; set; }
    [JsonProperty("itc_rev")]
    public List<ItcRev2> itc_rev { get; set; }
    [JsonProperty("itc_avl")]
    public List<ItcAvl2> itc_avl { get; set; }
}

public class TtVal2
{
    [JsonProperty("tt_csh_pd")]
    public int tt_csh_pd { get; set; }
    [JsonProperty("tt_pay")]
    public int tt_pay { get; set; }
    [JsonProperty("tt_itc_pd")]
    public int tt_itc_pd { get; set; }
}

public class Qn
{
    [JsonProperty("q1")]
    public string q1 { get; set; }
    [JsonProperty("q3")]
    public string q3 { get; set; }
    [JsonProperty("q2")]
    public string q2 { get; set; }
    [JsonProperty("q5")]
    public string q5 { get; set; }
    [JsonProperty("q4")]
    public string q4 { get; set; }
    [JsonProperty("q7")]
    public string q7 { get; set; }
    [JsonProperty("q6")]
    public string q6 { get; set; }
}

public class LtfeeDetails2
{
    [JsonProperty("csamt")]
    public int csamt { get; set; }
    [JsonProperty("iamt")]
    public int iamt { get; set; }
    [JsonProperty("samt")]
    public int samt { get; set; }
    [JsonProperty("camt")]
    public int camt { get; set; }
}

public class IntrDetails
{
    [JsonProperty("csamt")]
    public int csamt { get; set; }
    [JsonProperty("iamt")]
    public int iamt { get; set; }
    [JsonProperty("samt")]
    public int samt { get; set; }
    [JsonProperty("camt")]
    public int camt { get; set; }
}

public class IntrLtfee2
{
    [JsonProperty("ltfee_details")]
    public LtfeeDetails2 ltfee_details { get; set; }
    [JsonProperty("intr_details")]
    public IntrDetails intr_details { get; set; }
}

public class Detail2
{
    [JsonProperty("sup_details")]
    public SupDetails2 sup_details { get; set; }
    [JsonProperty("itc_elg")]
    public ItcElg2 itc_elg { get; set; }
    [JsonProperty("ret_period")]
    public string ret_period { get; set; }
    [JsonProperty("tt_val")]
    public TtVal2 tt_val { get; set; }
    [JsonProperty("qn")]
    public Qn qn { get; set; }
    [JsonProperty("intr_ltfee")]
    public IntrLtfee2 intr_ltfee { get; set; }
}

public class Gstr3b
{
    [JsonProperty("total")]
    public Total2 total { get; set; }
    [JsonProperty("details")]
    public List<Detail2> details { get; set; }
}

public class GSTTransactionResult
{
    [JsonProperty("profile")]
    public Profile profile { get; set; }
    [JsonProperty("filing_status")]
    public List<FilingStatu> filing_status { get; set; }
    [JsonProperty("transaction_summary")]
    public TransactionSummary transaction_summary { get; set; }
    [JsonProperty("gstr1")]
    public Gstr1 gstr1 { get; set; }
    [JsonProperty("Top3Cus")]
    public List<Top3Cus> Top3Cus { get; set; }
    [JsonProperty("gstin")]
    public string gstin { get; set; }
    [JsonProperty("Averages")]
    public Averages Averages { get; set; }
    [JsonProperty("gstr3b")]
    public Gstr3b gstr3b { get; set; }
}

public class GSTTransactionAPIResponse
{
    [JsonProperty("statusCode")]
    public int statusCode { get; set; }
    [JsonProperty("requestId")]
    public string requestId { get; set; }
    [JsonProperty("result")]
    public GSTTransactionResult result { get; set; }
}

}
