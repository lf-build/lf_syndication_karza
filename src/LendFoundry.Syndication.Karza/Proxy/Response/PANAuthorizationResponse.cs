using System.Collections.Generic;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Karza.Proxy
{
    public class PANAuthorizationResult
{
    [JsonProperty("name")]
    public string name { get; set; }
}

public class PANAuthorizationResponse
{
    [JsonProperty("result")]
    public PANAuthorizationResult result { get; set; }
    [JsonProperty("request_id")]
    public string request_id { get; set; }
    [JsonProperty("status-code")]
    public string status_code { get; set; }
}

}