using System.Collections.Generic;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Karza.Proxy
{
    
    public class Pradr
{
    [JsonProperty("em")]
    public string em { get; set; }
    [JsonProperty("adr")]
    public string adr { get; set; }
    [JsonProperty("addr")]
    public string addr { get; set; }
    [JsonProperty("mb")]
    public long mb { get; set; }
    [JsonProperty("ntr")]
    public string ntr { get; set; }
    [JsonProperty("lastUpdatedDate")]
    public string lastUpdatedDate { get; set; }
}

public class Adadr
{
    [JsonProperty("em")]
    public string em { get; set; }
     [JsonProperty("adr")]
    public string adr { get; set; }
     [JsonProperty("addr")]
    public string addr { get; set; }
     [JsonProperty("mb")]
    public object mb { get; set; }
     [JsonProperty("ntr")]
    public string ntr { get; set; }
     [JsonProperty("lastUpdatedDate")]
    public string lastUpdatedDate { get; set; }
}

public class Result
{
     [JsonProperty("mbr")]
    public List<string> mbr { get; set; }
     [JsonProperty("canFlag")]
    public string canFlag { get; set; }
     [JsonProperty("pradr")]
    public Pradr pradr { get; set; }
     [JsonProperty("tradeNam")]
    public string tradeNam { get; set; }
     [JsonProperty("contacted")]
    public string contacted { get; set; }
     [JsonProperty("gstin")]
    public string gstin { get; set; }
     [JsonProperty("nba")]
    public List<string> nba { get; set; }
     [JsonProperty("stjCd")]
    public string stjCd { get; set; }
     [JsonProperty("stj")]
    public string stj { get; set; }
     [JsonProperty("ppr")]
    public string ppr { get; set; }
     [JsonProperty("dty")]
    public string dty { get; set; }
     [JsonProperty("cmpRt")]
    public string cmpRt { get; set; }
     [JsonProperty("lstupdt")]
    public string lstupdt { get; set; }
     [JsonProperty("ctb")]
    public string ctb { get; set; }
     [JsonProperty("sts")]
    public string sts { get; set; }
     [JsonProperty("cxdt")]
    public string cxdt { get; set; }
     [JsonProperty("adadr")]
    public List<Adadr> adadr { get; set; }
     [JsonProperty("lgnm")]
    public string lgnm { get; set; }
     [JsonProperty("ctjCd")]
    public string ctjCd { get; set; }
     [JsonProperty("ctj")]
    public string ctj { get; set; }
     [JsonProperty("rgdt")]
     public string rgdt { get; set; }
}

public class GSTAuthorizationResponse: IGSTAuthorizationResponse
{
    [JsonProperty("result")]
    public Result result { get; set; }
    [JsonProperty("request-id")]
    public string request_id { get; set; }
    [JsonProperty("status-code")]
    public string status_code { get; set; }
}

}