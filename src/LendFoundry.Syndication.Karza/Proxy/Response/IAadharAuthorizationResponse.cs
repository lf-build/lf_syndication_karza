namespace LendFoundry.Syndication.Karza.Proxy
{
    public interface IAadharAuthorizationResponse
    {
        string status_code { get; set; }
        string request_id { get; set; }
        AadhaarResult result { get; set; }
    }
}