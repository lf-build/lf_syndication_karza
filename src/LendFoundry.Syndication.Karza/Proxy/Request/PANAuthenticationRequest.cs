using Newtonsoft.Json;

namespace LendFoundry.Syndication.Karza.Proxy.Request
{
    public class PANAuthenticationRequest : IPANAuthenticationRequest
    {
        public PANAuthenticationRequest()
        {

        }
        public PANAuthenticationRequest(LendFoundry.Syndication.Karza.Request.IPANAuthenticationRequest request)
        {
            pan = request.pan;
            consent = request.consent;
        }
        [JsonProperty("pan")]
        public string pan { get; set; }
        [JsonProperty("consent")]
        public string consent { get; set; }
    }
}