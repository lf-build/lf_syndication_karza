namespace LendFoundry.Syndication.Karza.Proxy.Request
{
    public interface IDateofbirth
    {
        string Format { get; set; }
        string DobType { get; set; }
        string DobValue { get; set; }
    }
}