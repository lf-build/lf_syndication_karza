using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Karza.Proxy.Request
{
    public class Demographics : IDemographics
    {
        public Demographics()
        {

        }
        public Demographics(LendFoundry.Syndication.Karza.Request.IDemographics request)
        {
            if (request != null)
            {
                Name = new Name(request.Name);
                Dob = new Dateofbirth(request.Dob);
                Phone = request.Phone;
                Gender = request.Gender;
                AddressFormat = request.AddressFormat;
                AddressFreetext = new AddressFreetext(request.AddressFreetext);
            }

        }

        [JsonProperty("name")]
        [JsonConverter(typeof(InterfaceConverter<IName, Name>))]
        public IName Name { get; set; }
        [JsonProperty("dob")]
        [JsonConverter(typeof(InterfaceConverter<IDateofbirth, Dateofbirth>))]
        public IDateofbirth Dob { get; set; }
        [JsonProperty("phone")]
        public string Phone { get; set; }
        [JsonProperty("gender")]
        public string Gender { get; set; }
        [JsonProperty("address-format")]
        public string AddressFormat { get; set; }
        [JsonProperty("address-freetext")]
        [JsonConverter(typeof(InterfaceConverter<IAddressFreetext, AddressFreetext>))]
        public IAddressFreetext AddressFreetext { get; set; }
    }


}