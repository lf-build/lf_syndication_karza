using Newtonsoft.Json;

namespace LendFoundry.Syndication.Karza.Proxy.Request
{

    public class Dateofbirth : IDateofbirth
    {
        public Dateofbirth()
        {

        }
        public Dateofbirth(LendFoundry.Syndication.Karza.Request.IDateofbirth request)
        {
            Format = request.Format;
            DobType = request.DobType;
        }
        [JsonProperty("format")]
        public string Format { get; set; }
        [JsonProperty("dob-type")]
        public string DobType { get; set; }
        [JsonProperty("dob-value")]
        public string DobValue { get; set; }
    }


}