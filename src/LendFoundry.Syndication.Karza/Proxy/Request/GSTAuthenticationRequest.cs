using Newtonsoft.Json;

namespace LendFoundry.Syndication.Karza.Proxy.Request
{
    public class GSTAuthenticationRequest : IGSTAuthenticationRequest
    {
        public GSTAuthenticationRequest()
        {

        }
        public GSTAuthenticationRequest(LendFoundry.Syndication.Karza.Request.IGSTAuthenticationRequest request)
        {
            gstin = request.gstin;
            consent = request.consent;
        }
        [JsonProperty("gstin")]
        public string gstin { get; set; }
        [JsonProperty("consent")]
        public string consent { get; set; }
    }
}