namespace LendFoundry.Syndication.Karza.Proxy.Request
{
    public interface IAadharAuthenticationRequest
    {
        string AadhaarId { get; set; }
        string Consent { get; set; }
        IDemographics Demographics { get; set; }
    }
}