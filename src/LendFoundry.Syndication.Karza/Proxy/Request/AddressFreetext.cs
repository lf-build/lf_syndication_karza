using Newtonsoft.Json;

namespace LendFoundry.Syndication.Karza.Proxy.Request
{



    public class AddressFreetext : IAddressFreetext
    {
        public AddressFreetext()
        {

        }
        public AddressFreetext(LendFoundry.Syndication.Karza.Request.IAddressFreetext request)
        {
            if (request != null)
            {
                MatchingStrategy = request.MatchingStrategy;
                MatchingValue = request.MatchingValue;
                AddressValue = request.AddressValue;
            }
        }
        [JsonProperty("matching-strategy")]
        public string MatchingStrategy { get; set; }
        [JsonProperty("matching-value")]
        public string MatchingValue { get; set; }
        [JsonProperty("address-value")]
        public string AddressValue { get; set; }
    }


}