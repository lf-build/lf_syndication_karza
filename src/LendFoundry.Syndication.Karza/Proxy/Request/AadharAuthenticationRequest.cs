using Newtonsoft.Json;

namespace LendFoundry.Syndication.Karza.Proxy.Request
{
    public class AadharAuthenticationRequest : IAadharAuthenticationRequest
    {
        public AadharAuthenticationRequest()
        {

        }
        public AadharAuthenticationRequest(LendFoundry.Syndication.Karza.Request.IAadharAuthenticationRequest request)
        {
            AadhaarId=request.AadhaarId;
            Consent=request.Consent;
            Demographics=new Demographics(request.Demographics);
        }
        [JsonProperty("aadhaar-id")]
        public string AadhaarId { get; set; }
        [JsonProperty("consent")]
        public string Consent { get; set; }
        [JsonProperty("demographics")]
        public IDemographics Demographics { get; set; }
    }


}