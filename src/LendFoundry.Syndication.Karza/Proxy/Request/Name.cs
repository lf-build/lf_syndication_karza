using Newtonsoft.Json;

namespace LendFoundry.Syndication.Karza.Proxy.Request
{

    public class Name : IName
    {
        public Name()
        {
            
        }
         public Name(LendFoundry.Syndication.Karza.Request.IName request)
        {
            MatchingStrategy=request.MatchingStrategy;
            MatchingValue=request.MatchingValue;
            NameValue=request.NameValue;
        }
        [JsonProperty("matching-strategy")]
        public string MatchingStrategy { get; set; }
        [JsonProperty("matching-value")]
        public string MatchingValue { get; set; }
        [JsonProperty("name-value")]
        public string NameValue { get; set; }
    }


}