namespace LendFoundry.Syndication.Karza.Proxy.Request
{
    public interface IPANAuthenticationRequest
    {
         string pan{get;set;}
         string consent{get;set;}
    }
}