namespace LendFoundry.Syndication.Karza.Proxy.Request
{
    public interface IDemographics
    {
        IName Name { get; set; }
        IDateofbirth Dob { get; set; }
        string Phone { get; set; }
        string Gender { get; set; }
        string AddressFormat { get; set; }
        IAddressFreetext AddressFreetext { get; set; }
    }
}