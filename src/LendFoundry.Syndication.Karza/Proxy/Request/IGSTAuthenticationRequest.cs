namespace LendFoundry.Syndication.Karza.Proxy.Request
{
    public interface IGSTAuthenticationRequest
    {
         string gstin{get;set;}
         string consent{get;set;}
    }
}