﻿using System.Threading.Tasks;
using LendFoundry.Syndication.Karza.Request;

namespace LendFoundry.Syndication.Karza.Proxy
{
    public interface IKarzaProxy
    {
      Task<IGSTAuthorizationResponse> GSTAuthorizationDetail(IGSTAuthenticationRequest gstauthenticationrequest);
      Task<PANAuthorizationResponse> PANAuthenticationDetail(IPANAuthenticationRequest panuthenticationrequest);
      Task<IAadharAuthorizationResponse> AadharAuthenticationDetail(IAadharAuthenticationRequest aadharauthenticationRequest);
      Task<GSTTransactionAPIResponse> GSTTransactionAPIDetails(IGSTAuthenticationRequest gsttransactionrequest);
      
      
    }
}