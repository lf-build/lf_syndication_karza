﻿using LendFoundry.Syndication.Karza.Request;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Net;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Karza.Proxy
{
    public class KarzaProxy : IKarzaProxy
    {
        private IKarzaServiceConfiguration KarzaServiceConfiguration { get; }

        public KarzaProxy(IKarzaServiceConfiguration karzaserviceconfiguration)
        {
            if (karzaserviceconfiguration == null)
                throw new ArgumentNullException(nameof(karzaserviceconfiguration));
            if (string.IsNullOrWhiteSpace(karzaserviceconfiguration.BaseUrl))
                throw new ArgumentException("Url is required", nameof(karzaserviceconfiguration.BaseUrl));
            if (string.IsNullOrWhiteSpace(karzaserviceconfiguration.ApiKey))
                throw new ArgumentException("ApiKey is required", nameof(karzaserviceconfiguration.ApiKey));
            KarzaServiceConfiguration = karzaserviceconfiguration;
        }


        public async Task<IGSTAuthorizationResponse> GSTAuthorizationDetail(IGSTAuthenticationRequest gstauthenticationrequest)
        {
            if (gstauthenticationrequest == null)
                throw new ArgumentNullException(nameof(gstauthenticationrequest));
            var client = new RestClient(KarzaServiceConfiguration.BaseUrl + "/" + KarzaServiceConfiguration.ServiceUrls["GSTAuthenticationUrl"]);
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("x-karza-key", KarzaServiceConfiguration.ApiKey);
            request.AddJsonBody(gstauthenticationrequest);
            var objectResponse = await ExecuteRequestAsync<GSTAuthorizationResponse>(client, request);
            return objectResponse;
        }


        public async Task<PANAuthorizationResponse> PANAuthenticationDetail(IPANAuthenticationRequest panuthenticationrequest)
        {
            if (panuthenticationrequest == null)
                throw new ArgumentNullException(nameof(panuthenticationrequest));
            var client = new RestClient(KarzaServiceConfiguration.BaseUrl + "/" + KarzaServiceConfiguration.ServiceUrls["PANAuthenticationUrl"]);
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("x-karza-key", KarzaServiceConfiguration.ApiKey);
            request.AddJsonBody(panuthenticationrequest);
            var objectResponse = await ExecuteRequestAsync<PANAuthorizationResponse>(client, request);
            return objectResponse;
        }

        public async Task<IAadharAuthorizationResponse> AadharAuthenticationDetail(IAadharAuthenticationRequest aadharauthenticationRequest)
        {
            if (aadharauthenticationRequest == null)
                throw new ArgumentNullException(nameof(aadharauthenticationRequest));
            var client = new RestClient(KarzaServiceConfiguration.BaseUrl + "/" + KarzaServiceConfiguration.ServiceUrls["AadharAuthenticationUrl"]);
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("x-karza-key", KarzaServiceConfiguration.ApiKey);
            request.AddJsonBody(aadharauthenticationRequest);
            var objectResponse = await ExecuteRequestAsync<AadharAuthorizationResponse>(client, request);
            return objectResponse;
        }

        public async Task<GSTTransactionAPIResponse> GSTTransactionAPIDetails(IGSTAuthenticationRequest gsttransactionrequest)
        {
            if (gsttransactionrequest == null)
                throw new ArgumentNullException(nameof(gsttransactionrequest));
            var client = new RestClient(KarzaServiceConfiguration.BaseUrl + "/" + KarzaServiceConfiguration.ServiceUrls["GSTTransactionAPIUrl"]);
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("x-karza-key", KarzaServiceConfiguration.ApiKey);
            request.AddJsonBody(gsttransactionrequest);
            var objectResponse = await ExecuteRequestAsync<GSTTransactionAPIResponse>(client, request);
            return objectResponse;
        }


        private async Task<T> ExecuteRequestAsync<T>(IRestClient client, IRestRequest request) where T : class
        {
            var response = await client.ExecuteTaskAsync(request);
            if (response == null)
                throw new ArgumentNullException(nameof(response));

            if (response.ErrorException != null)
                throw new KarzaException("Service call failed", response.ErrorException);


            if (response.ResponseStatus != ResponseStatus.Completed)
                throw new KarzaException(
                    $"Service call failed. Status {response.ResponseStatus}. Response: {response.Content ?? ""}");

            if (response.ResponseStatus == ResponseStatus.Completed)
            {
                if (response.StatusCode.ToString().ToLower() == "methodnotallowed")
                    throw new KarzaException(
                        $"Service call failed. Status {response.ResponseStatus}. Response: {response.StatusDescription ?? ""}");
            }

            return JsonConvert.DeserializeObject<T>(response.Content);
        }
    }
}
