﻿using LendFoundry.Foundation.Client;
using LendFoundry.Security.Tokens;
using System;

using Microsoft.Extensions.DependencyInjection;

namespace LendFoundry.Syndication.Karza.Client
{
    public static class KarzaServiceClientExtensions
    {
        [Obsolete("Need to use the overloaded with Uri")]
        public static IServiceCollection AddKarzaService(this IServiceCollection services, string endpoint, int port)
        {
            services.AddTransient<IKarzaServiceClientFactory>(p => new KarzaServiceClientFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<IKarzaServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
        public static IServiceCollection AddKarzaService(this IServiceCollection services, Uri uri)
        {
            services.AddSingleton<IKarzaServiceClientFactory>(p => new KarzaServiceClientFactory(p, uri));
            services.AddSingleton(p => p.GetService<IKarzaServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddKarzaService(this IServiceCollection services)
        {
            services.AddSingleton<IKarzaServiceClientFactory>(p => new KarzaServiceClientFactory(p));
            services.AddSingleton(p => p.GetService<IKarzaServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
    }
}
