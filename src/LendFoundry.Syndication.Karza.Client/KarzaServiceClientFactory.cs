﻿using System;
using LendFoundry.Security.Tokens;
using LendFoundry.Syndication.Karza;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Client;
using Microsoft.Extensions.DependencyInjection;
using LendFoundry.Karza.Client;

namespace LendFoundry.Syndication.Karza.Client
{
    public class KarzaServiceClientFactory :IKarzaServiceClientFactory
    {
        [Obsolete("Need to use the overloaded with Uri")]
        public KarzaServiceClientFactory(IServiceProvider provider, string endpoint, int port)
        {
            Provider = provider;
            Endpoint = endpoint;
            Port = port;
            Uri = new UriBuilder("http", endpoint, port).Uri;
        }
        public KarzaServiceClientFactory(IServiceProvider provider, Uri uri = null)
        {
            Provider = provider;
            Uri = uri;
        }
        private IServiceProvider Provider { get; }

        private string Endpoint { get; }

        private int Port { get; }
        private Uri Uri { get; }


        public IKarzaService Create(ITokenReader reader)
        {
            var uri = Uri;
            if (uri == null)
            {
                var logger = Provider.GetService<ILoggerFactory>().Create(NullLogContext.Instance);
                uri = Provider.GetRequiredService<IDependencyServiceUriResolverFactory>().Create(reader, logger).Get("karza");
            }
            var client = Provider.GetServiceClient(reader, uri);
            return new KarzaClientService(client);
        }
    }
}
