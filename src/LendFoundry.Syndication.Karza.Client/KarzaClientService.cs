using System.Threading.Tasks;
using LendFoundry.Foundation.Client;
using RestSharp;
using LendFoundry.Syndication.Karza;
using LendFoundry.Syndication.Karza.Request;
using LendFoundry.Syndication.Karza.Response;
using System;

namespace LendFoundry.Karza.Client
{
    public class KarzaClientService : IKarzaService
    {
        #region Public Constructors

        /// <summary>
        /// Initializes a new instance of the KarzaService class.
        /// </summary>
        /// <param name="client">The client.</param>
        public KarzaClientService(IServiceClient client)
        {
            Client = client;
        }

        #endregion Public Constructors

        #region Private Properties

        /// <summary>
        /// Gets the client.
        /// </summary>
        /// <value>The client.</value>
        private IServiceClient Client { get; }


        #endregion Private Properties

        #region Public Method

        /// <summary>
        /// Verifies the lead.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="entityId">The entity identifier.</param>
        /// <param name="leadRequest">The lead request.</param>
        /// <returns>LeadVerification Response.</returns>
        
        public async Task<IGSTAuthorizationResponse> GSTAuthorizationDetail(string entityType, string entityId, IGSTAuthenticationRequest gstauthenticationrequest)
        {
            var request = new RestRequest("{entitytype}/{entityid}/gstdetailed", Method.POST);
            request.AddUrlSegment("entitytype", entityType);
            request.AddUrlSegment("entityid", entityId);
            request.AddJsonBody(gstauthenticationrequest);
            return await Client.ExecuteAsync<GSTAuthorizationResponse>(request);
        }

        public async Task<IPANAuthorizationResponse> PANAuthenticationDetail(string entityType, string entityId, IPANAuthenticationRequest panuthenticationrequest)
        {
            var request = new RestRequest("{entitytype}/{entityid}/pan", Method.POST);
            request.AddUrlSegment("entitytype", entityType);
            request.AddUrlSegment("entityid", entityId);
            request.AddJsonBody(panuthenticationrequest);
            return await Client.ExecuteAsync<PANAuthorizationResponse>(request);
        }
         
        public async Task<IAadharAuthorizationResponse> AadharAuthenticationDetail(string entityType, string entityId, IAadharAuthenticationRequest aadharauthenticationrequest)
         {
            var request = new RestRequest("{entitytype}/{entityid}/aadhar", Method.POST);
            request.AddUrlSegment("entitytype", entityType);
            request.AddUrlSegment("entityid", entityId);
            request.AddJsonBody(aadharauthenticationrequest);
            return await Client.ExecuteAsync<AadharAuthorizationResponse>(request);
        }

         public async Task<Syndication.Karza.Proxy.GSTTransactionAPIResponse> GSTTransactionAPIDetails(string entityType, string entityId,IGSTAuthenticationRequest gsttransactionrequest)
        {
            var request = new RestRequest("{entitytype}/{entityid}/gst", Method.POST);
            request.AddUrlSegment("entitytype", entityType);
            request.AddUrlSegment("entityid", entityId);
            request.AddJsonBody(gsttransactionrequest);
            return await Client.ExecuteAsync<Syndication.Karza.Proxy.GSTTransactionAPIResponse>(request);
        }
        #endregion Public Method
    }
}