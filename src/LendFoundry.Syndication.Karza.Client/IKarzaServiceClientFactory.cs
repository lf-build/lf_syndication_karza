﻿using LendFoundry.Security.Tokens;
using LendFoundry.Syndication.Karza;

namespace LendFoundry.Syndication.Karza.Client
{
   public interface IKarzaServiceClientFactory
    {
        IKarzaService Create(ITokenReader reader);
    }
}
