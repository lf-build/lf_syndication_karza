﻿using System;
#if DOTNET2
using Microsoft.AspNetCore.Mvc;
#else
using Microsoft.AspNet.Mvc;
#endif
using System.Threading.Tasks;
using LendFoundry.Foundation.Services;
using LendFoundry.Syndication.Karza.Request;
using LendFoundry.Foundation.Logging;
using LendFoundry.Syndication.Karza.Proxy;

namespace LendFoundry.Syndication.Karza.Api.Controllers
{
    /// <summary>
    /// Karza ApiController
    /// </summary>
    [Route("/")]
    public class ApiController : ExtendedController
    {
        /// <summary>
        /// Karza ApiController constructor
        /// </summary>
        /// <param name="karzaService"></param>
        /// <param name="logger"></param>
        /// <returns></returns>
        public ApiController(IKarzaService karzaService,ILogger logger):base(logger)
        {
            if (karzaService == null)
                throw new ArgumentNullException(nameof(karzaService));
            KarzaService = karzaService;
        }

        private IKarzaService KarzaService { get; }

        /// <summary>
        /// GSTAuthorizationDetail
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="gstauthenticationrequest"></param>
        /// <returns></returns>
        [HttpPost("{entitytype}/{entityid}/gstdetailed")]
        [ProducesResponseType(typeof(IGSTAuthorizationResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GSTAuthorizationDetail(string entityType, string entityId,[FromBody]GSTAuthenticationRequest gstauthenticationrequest)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await KarzaService.GSTAuthorizationDetail(entityType, entityId, gstauthenticationrequest));
                }
                catch (KarzaException exception)
                {
                    return ErrorResult.BadRequest(exception.Message);
                }
            });
        }

        /// <summary>
        /// PANAuthenticationDetail
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="panauthenticationrequest"></param>
        /// <returns></returns>
        [HttpPost("{entitytype}/{entityid}/pan")]
        [ProducesResponseType(typeof(PANAuthorizationResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> PANAuthenticationDetail(string entityType, string entityId,[FromBody]PANAuthenticationRequest panauthenticationrequest)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await KarzaService.PANAuthenticationDetail(entityType, entityId, panauthenticationrequest));
                }
                catch (KarzaException exception)
                {
                    return ErrorResult.BadRequest(exception.Message);
                }
            });
        }

        /// <summary>
        /// AadharAuthenticationDetail
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="panauthenticationrequest"></param>
        /// <returns></returns>
        [HttpPost("{entitytype}/{entityid}/aadhar")]
        [ProducesResponseType(typeof(IAadharAuthorizationResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> AadharAuthenticationDetail(string entityType, string entityId,[FromBody]AadharAuthenticationRequest panauthenticationrequest)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await KarzaService.AadharAuthenticationDetail(entityType, entityId, panauthenticationrequest));
                }
                catch (KarzaException exception)
                {
                    return ErrorResult.BadRequest(exception.Message);
                }
            });
        }

        /// <summary>
        /// GSTTransactionAPIDetails
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="gstauthenticationrequest"></param>
        /// <returns></returns>
        [HttpPost("{entitytype}/{entityid}/gst")]
        [ProducesResponseType(typeof(IGSTAuthorizationResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GSTTransactionAPIDetails(string entityType, string entityId,[FromBody]GSTAuthenticationRequest gstauthenticationrequest)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await KarzaService.GSTTransactionAPIDetails(entityType, entityId, gstauthenticationrequest));
                }
                catch (KarzaException exception)
                {
                    return ErrorResult.BadRequest(exception.Message);
                }
            });
        }
    }
}
