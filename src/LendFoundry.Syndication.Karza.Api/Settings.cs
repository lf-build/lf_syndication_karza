﻿using LendFoundry.Foundation.Services.Settings;
using System;

namespace LendFoundry.Syndication.Karza.Api
{
    /// <summary>
    /// 
    /// </summary>
    public class Settings
    {
       /// <summary>
       /// 
       /// </summary>
       /// <returns></returns>
       public static string ServiceName => Environment.GetEnvironmentVariable($"CONFIGURATION_NAME") ?? "karza";
    }
}
